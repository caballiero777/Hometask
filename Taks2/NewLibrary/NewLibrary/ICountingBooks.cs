﻿namespace NewLibrary
{
    public interface ICountingBooks
    {
        int GetNumberOfBooks();
    }
}