﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewLibrary
{
    class Library : ICountingBooks
    {
        public ITDepartment ITSection { get { return new ITDepartment(); } }
        public FantasyDepartment FantasySection { get { return new FantasyDepartment(); } }
        public AdventureDepartment AdventureSection { get { return new AdventureDepartment(); } }
        public int GetNumberOfBooks()
        {
            return ITSection.books.Count +
                   FantasySection.books.Count +
                   AdventureSection.books.Count;
        }
    }
}
