﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NewLibrary
{
    public abstract class Department : IComparable, ICountingBooks
    {
        public List<Book> books;
        public abstract string NameOfSection { get; }
        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            Department comparer = obj as Department;
            if (comparer != null)
            {
                return books.Count.CompareTo(comparer.books.Count);
            }
            return 1;
        }
        public int GetNumberOfBooks()
        {
            return books.Count;
        }
        public override string ToString()
        {
            return $"Section name:{ NameOfSection }";
        }
        public void SortBooks()
        {
            books = books.OrderBy(x => x.PagesCount).ToList();
        }
        public Book SearchBook(string name)
        {
            return books.FirstOrDefault(x => x.Name == name);
        }
    }
}
