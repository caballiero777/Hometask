﻿using System;

namespace NewLibrary
{
    public class Book : IComparable
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private int year;
        public int Year
        {
            get { return year; }
            set { year = value; }
        }
        private int pagesCount;
        public int PagesCount
        {
            get { return pagesCount; }
            set { pagesCount = value; }
        }
        private Author author;
        public Author Author
        {
            get { return author; }
            set { author = value; }
        }
        public Book() { }
        public Book(string name, int year, Author author, int pagesCount)
        {
            this.name = name;
            this.year = year;
            this.author = author;
            this.pagesCount = pagesCount;
        }
        public override string ToString()
        {
            return $" Book name:{Name}\n Year: {Year}\n {Author.ToString()}\n Number of pages: {PagesCount}";
        }
        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            Book comparer = obj as Book;
            if (comparer != null)
            {
                return PagesCount.CompareTo(comparer.PagesCount);
            }
            return 1;
        }
    }
}
