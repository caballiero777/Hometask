﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NewLibrary
{
    static class XMLHelper
    {
        public static List<Book> ReadXML(string path)
        {
            XDocument xDoc = XDocument.Load(path);
            List<Book> contentOfXml = new List<Book>();
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement root = xDoc.Element("library");
                if (root != null && root.HasElements)
                {
                    foreach (var department in root.Elements("department"))
                    {
                        foreach (var book in department.Elements("book"))
                        {
                            var bookName = book.Attribute("name").Value;
                            var bookYear = book.Attribute("year").Value;
                            var bookAuthor = book.Attribute("author").Value;
                            var bookPages = book.Attribute("pages").Value;
                            contentOfXml.Add(new Book(bookName, int.Parse(bookYear), new Author(bookAuthor), int.Parse(bookPages)));
                        }
                    }
                }
            }
            return contentOfXml;
        }
        public static void AddBookToXML(string path, string departmentName, Book book)
        {
            XDocument xDoc = XDocument.Load(path);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement root = xDoc.Element("library");
                if (root != null && root.HasElements)
                {
                    var department = root.Elements("department").FirstOrDefault(x => x.Attribute("name").Value == departmentName);
                    if (department != null)
                    {
                        XElement bookToAdd = new XElement("book");
                        bookToAdd.Add(new XAttribute("name", book.Name));
                        bookToAdd.Add(new XAttribute("year", book.Year));
                        bookToAdd.Add(new XAttribute("author", book.Author.Name));
                        bookToAdd.Add(new XAttribute("pages", book.PagesCount));
                        department.Add(bookToAdd);
                        FileStream stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                        xDoc.Save(stream);
                    }
                }
            }
            
        }
        public static void ModifyXML(string path, string bookName, string attribute, string newValue)
        {
            XDocument xDoc = XDocument.Load(path);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement root = xDoc.Element("library");
                if (root != null && root.HasElements)
                {
                    foreach (var department in root.Elements("department"))
                    {
                        var book = department.Elements("book").FirstOrDefault(x => x.Attribute("name").Value == bookName);
                        if(book != null)
                        {
                            book.Attribute(attribute).Value = newValue;
                        }
                        break;
                    }
                    xDoc.Save(path);
                }
            }
        }
        public static void DeleteBook(string path, string bookName)
        {
            XDocument xDoc = XDocument.Load(path);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement root = xDoc.Element("library");
                if (root != null && root.HasElements)
                {
                    foreach (var department in root.Elements("department"))
                    {
                        var book = department.Elements("book").FirstOrDefault(x => x.Attribute("name").Value == bookName);
                        if (book != null)
                        {
                            book.Remove();
                        }
                        break;
                    }
                    xDoc.Save(path);
                }
            }
        }
    }
}
