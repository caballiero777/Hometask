﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdditionalTask2
{
    class Program
    {
        static void PrintMatrix(int[][] matrix)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[0].Length; j++)
                {
                    Console.Write(matrix[i][j] + " ");
                }
                Console.WriteLine();
            }
        }
        static int? FindFirstNonNegativeColumn(int[,] matrix)
        {
            bool isNegative = false;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                isNegative = false;
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[j, i] < 0)
                    {
                        isNegative = true;
                        break;
                    }
                }
                if (isNegative == false)
                    return i;
            }
            return null;
        }
        static int[][] SortMatrixRows(int[][] matrix)
        {
            Hashtable similarCounter = new Hashtable();
            Dictionary<int, int> rowCounter = new Dictionary<int, int>();
            int counter = 0;
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[0].Length; j++)
                {
                    if (similarCounter.ContainsKey(matrix[i][j]))
                    {
                        counter++;
                    }
                    else
                    {
                        similarCounter.Add(matrix[i][j], j);
                    }
                }
                rowCounter.Add(i, counter);
                counter = 0;
                similarCounter.Clear();
            }
            int[][] outputMatrix = new int[matrix.GetLength(0)][];
            for (int i = 0; i < outputMatrix.Length; i++)
            {
                outputMatrix[i] = new int[matrix.GetLength(0)];
            }
            var orderedSet = rowCounter.OrderBy(x => x.Value);
            int index = 0;
            foreach (var item in orderedSet)
            {
                outputMatrix[index] = matrix[item.Key];
                index++;
            }
            return outputMatrix;
        }
        static void Main(string[] args)
        {
            int[,] matrix = new int[,]
            {
                { 4, 2, 3 },
                { -7, -5, 3 },
                { 10, 10, 2 }
            };
            int[][] m = new int[4][]
            {
                new int[] { 4, 2, 2, 2 },
                new int[] { 0, 7, 3, 9},
                new int[] { 1, 1, 5, 7},
                new int[] { 1, 15, 5, 7}
            };
            Console.WriteLine($"Firs column without negative number: {FindFirstNonNegativeColumn(matrix)}");
            Console.WriteLine("Ordered matrix:");
            PrintMatrix(SortMatrixRows(m));
            Console.ReadKey();
        }
    }
}
