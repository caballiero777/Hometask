﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdditionalTask4
{
    class Program
    {
        static bool CheckNumbers(int[,] matrix, int k)
        {
            for (int i = 0; i < 8; i++)
            {
                if (matrix[k, i] != matrix[i, k])
                {
                    return false;
                }
            }
            return true;
        }
        static void Main(string[] args)
        {
            int[,] matrix = new int[,]
            {
                { 0, 1, 2, 3, 4, 5, 6, 7},
                { 1, 4, 8, 9, 1, 2, 3, 7},
                { 2, 7, 2, 9, 3, 7, 1, 0},
                { 3, 7, 2, 2, 0, 7, 3, 4},
                { 4, 5, 2, 7, 3, 7, 1, 1},
                { 5, 7, 0, 1, 2, 3, 7, 0},
                { 6, 9, 8, 9, 7, 1, 2, 0},
                { 7, 7, 0, 4, 1, 0, 0, 9}
            };
            for (int i = 0; i < 8; i++)
            {
                Console.WriteLine("k = "+ i + " " + CheckNumbers(matrix, i));
            }
            Console.ReadKey();
        }
    }
}
