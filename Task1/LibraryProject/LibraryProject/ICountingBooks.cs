﻿namespace LibraryProject
{
    interface ICountingBooks
    {
        int GetNumberOfBooks();
    }
}
