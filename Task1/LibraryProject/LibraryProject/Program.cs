﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace LibraryProject
{
    class Program
    {
        static Author GetMostPublishedAuthor(params Department[] sections)
        {
            Author maxPublishes = sections[0].books[0].Author;
            for (int i = 0; i < sections.Length; i++)
            {
                for (int j = 0; j < sections[i].books.Length; j++)
                {
                    if (sections[i].books[j].Author.CompareTo(maxPublishes) == 1)
                    {
                        maxPublishes = sections[i].books[j].Author;
                    }
                }
            }
            return maxPublishes;
        }
        static Department GetBiggestSection(params Department[] sections)
        {
            int maxNumber = sections[0].GetNumberOfBooks();
            int index = 0;
            for (int i = 0; i < sections.Length; i++)
            {
                if (sections[i].GetNumberOfBooks() > maxNumber)
                {
                    maxNumber = sections[i].GetNumberOfBooks();
                    index = i;
                }
            }
            return sections[index];
        }
        static Book GetSmallestBook(params Department[] sections)
        {
            Book smallestBook = sections[0].books[0];
            for (int i = 0; i < sections.Length; i++)
            {
                for (int j = 0; j < sections[i].books.Length; j++)
                {
                    if (sections[i].books[j].CompareTo(smallestBook) == -1)
                    {
                        smallestBook = sections[i].books[j];
                    }
                }
            }
            return smallestBook;
        }

        static void Main(string[] args)
        {
            Library lib = new Library();

            Author mostPublishedAuthor = GetMostPublishedAuthor(lib.AdventureSection,
                                                                lib.FantasySection,    
                                                                lib.ITSection);
            Console.WriteLine("Author with biggest number of books:");
            Console.WriteLine(mostPublishedAuthor.ToString() + Environment.NewLine);

            Department biggestSection = GetBiggestSection(lib.AdventureSection,
                                                       lib.FantasySection,
                                                       lib.ITSection);
            Console.WriteLine("The biggest section: ");
            Console.WriteLine(biggestSection.ToString() + Environment.NewLine);

            Book smallestBook = GetSmallestBook(lib.AdventureSection,
                                                      lib.FantasySection,
                                                      lib.ITSection);
            Console.WriteLine("Book with the lowest amount of pages: ");
            Console.WriteLine(smallestBook.ToString() + Environment.NewLine);
            Console.ReadKey();
        }
    }
}
