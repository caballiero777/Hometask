﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryProject
{
    public class ITDepartment : Department
    {
        public override string NameOfSection
        {
            get
            {
                return "IT Section";
            }
        }

        public ITDepartment()
        {
            books = new Book[] {
                new Book
                {
                    Name = "CLR via C#",
                    Year = 2006,
                    Author = new Author { Name = "Jeffrey Richter", NumberOfPublishedBooks = 13 },
                    PagesCount = 896
                },

                new Book
                {
                    Name = "C# Practices",
                    Year = 2009,
                    Author = new Author { Name = "John Doe", NumberOfPublishedBooks = 5 },
                    PagesCount = 520
                },

                new Book
                {
                    Name = "C# 6.0 and the .NET 4.6 Framework",
                    Year = 2015,
                    Author = new Author { Name = "Andrew Troelsen", NumberOfPublishedBooks = 17 },
                    PagesCount = 1310
                },

                new Book
                {
                    Name = "C# Cookbook",
                    Year = 2016,
                    Author = new Author { Name = "Jay Hilyard", NumberOfPublishedBooks = 6 },
                    PagesCount = 958
                }

            };
        }
    }
}
