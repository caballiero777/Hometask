﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    public class AdventureDepartment : Department
    {
        public AdventureDepartment()
        {
            books = new List<Book>() {
                new Book
                {
                    Name = "Treasure Island",
                    Year = 1882,
                    Author = new Author { Name = "Robert Louis Stevenson", NumberOfPublishedBooks = 32 },
                    PagesCount = 813
                },

                new Book
                {
                    Name = "The Hobbit",
                    Year = 1937,
                    Author = new Author { Name = "J. R. R. Tolkien", NumberOfPublishedBooks = 12 },
                    PagesCount = 1400
                },

                new Book
                {
                    Name = "20,000 Leagues Under the Sea",
                    Year = 1870,
                    Author = new Author { Name = "Jules Verne", NumberOfPublishedBooks = 35 },
                    PagesCount = 904
                }
            };
        }
        public AdventureDepartment(Book book)
        {
            books.Add(book);
        }
        public override string NameOfSection
        {
            get
            {
                return "Adventure Section";
            }
        }
    }
}
