﻿namespace Task3
{
    public interface ICountingBooks
    {
        int GetNumberOfBooks();
    }
}