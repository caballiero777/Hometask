﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    public class FantasyDepartment : Department
    {
        public FantasyDepartment()
        {
            books = new List<Book>() {
                new Book
                {
                    Name = "The Chronicles of Narnia",
                    Year = 1950,
                    Author = new Author { Name = "C.S. Lewis", NumberOfPublishedBooks = 21 },
                    PagesCount = 750
                },

                new Book
                {
                    Name = "The Dark Tower",
                    Year = 1982,
                    Author = new Author { Name = "Stephen King", NumberOfPublishedBooks = 60 },
                    PagesCount = 700
                }

            };
        }
        public FantasyDepartment(Book book)
        {
            books.Add(book);
        }
        public override string NameOfSection
        {
            get
            {
                return "Fantasy Section";
            }
        }
    }
}
