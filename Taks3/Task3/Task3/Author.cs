﻿using System;

namespace Task3
{
    public class Author : IComparable, ICountingBooks
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private int numberOfPublishedBooks;
        public int NumberOfPublishedBooks
        {
            get { return numberOfPublishedBooks; }
            set { numberOfPublishedBooks = value; }
        }
        public Author() { }
        public Author(string name) : this(name, 0)
        {
            this.name = name;
        }
        public Author(string name, int numberOfPublishedBooks)
        {
            this.numberOfPublishedBooks = numberOfPublishedBooks;
            this.name = name;
        }
        public override string ToString()
        {
            return $"Author name: {Name}, Number of published books: {numberOfPublishedBooks}";
        }
        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            Author comparer = obj as Author;
            if (comparer != null)
            {
                return NumberOfPublishedBooks.CompareTo(comparer.NumberOfPublishedBooks);
            }
            return 1;
        }
        public int GetNumberOfBooks()
        {
            return NumberOfPublishedBooks;
        }
    }
}