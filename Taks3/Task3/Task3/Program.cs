﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft;
using Newtonsoft.Json;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library();
            Console.WriteLine("Serializing all Library to JSON");
            string jsonLibrary = JsonConvert.SerializeObject(library);
            Console.WriteLine("Result of serialization: " + Environment.NewLine);
            Console.WriteLine(jsonLibrary);
            Library deserializedLibrary = JsonConvert.DeserializeObject<Library>(jsonLibrary);
            Console.WriteLine(Environment.NewLine + "Method from deserialized object:");
            Console.WriteLine("Number of books in library: " + deserializedLibrary.GetNumberOfBooks());
            Console.ReadKey();
        }
    }
}
