﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task5.Model;

namespace Task5.Interfaces
{
    interface IView 
    {
        void Close();
        User InitializeModel();
    }
}
