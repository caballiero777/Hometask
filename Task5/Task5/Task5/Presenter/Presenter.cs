﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task5.Interfaces;
using Task5.Model;

namespace Task5
{
    class Presenter
    {
        private IView view;
        private User user;
        public event EventHandler<User> UserAdded;
        public Presenter(IView view)
        {
            this.view = view;
            user = new User();
        }
        private bool IsModelValid(User user)
        {
            var context = new ValidationContext(user);
            var errorList = new List<ValidationResult>();
            if (!Validator.TryValidateObject(user, context, errorList, true))
            {
                foreach (var item in errorList)
                {
                    System.Windows.Forms.MessageBox.Show(item.ErrorMessage);
                }
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SerializeToCSV(ISerializer serializer, string path)
        {
            user = view.InitializeModel();
            if (IsModelValid(user))
            {
                serializer.Serialize<User>(user, path);
                OnChanged(user);
            }
        }
        public List<User> DeserializeFromCSV(ISerializer deserializer, string path)
        {
            var users = deserializer.Deserialize(path);
            return users;
        }
        private void OnChanged(User user)
        {
            UserAdded(this, user);
        }
        public void CloseView()
        {
            view.Close();
        }
    }
}
