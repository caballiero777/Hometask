﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
namespace Task5.Model
{
    public class User
    {
        [Required(ErrorMessage = "First Name is required")]
        [RegularExpression("[A-Za-z]{2,50}", ErrorMessage = "Only letters allowed")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")]
        [RegularExpression("[A-Za-z]{2,100}", ErrorMessage = "Only letters allowed")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Age is required")]
        [Range(1, 300, ErrorMessage = "Age must be between 1 and 300")]
        public int Age { get; set; }
        [Required(ErrorMessage = "Place of work is required")]
        [MaxLength(255, ErrorMessage = "Place of work must be between 1 and 255 characters")]
        public string PlaceOfWork { get; set; }
        public override string ToString()
        {
            return $"Name: {FirstName} {LastName}\n Age: {Age}\n Place of work: {PlaceOfWork}";
        }
    }
}
