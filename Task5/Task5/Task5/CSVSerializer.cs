﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task5.Interfaces;
using Task5.Model;

namespace Task5
{
    class CSVSerializer : ISerializer
    {
        public void Serialize<T>(T value, string path)
        {
            using (var stream = new FileStream(path, FileMode.Append, FileAccess.Write))
            {
                using (var writer = new StreamWriter(stream))
                {
                    var builder = new StringBuilder();
                    var type = value.GetType();
                    foreach (var item in type.GetProperties())
                    {
                        builder.Append(item.GetValue(value) + ",");
                    } 
                    writer.WriteLine(builder.ToString());
                }
            }
        }
        public List<User> Deserialize(string path)
        {
            var result = new List<User>();
            var lines = File.ReadAllLines(path);
            User user = null;
            foreach (var line in lines)
            {
                var properties = line.Split(',');
                user = new User()
                {
                    FirstName = properties[0],
                    LastName = properties[1],
                    Age = Convert.ToInt32(properties[2]),
                    PlaceOfWork = properties[3]
                };
                result.Add(user);
            }
            return result;
        }
    }
}
